# WDWM

 wdwm has been rewritten with cleaner code, less crust and less bugs, see https://gitlab.com/konk0418/wdwm2

Wills build of DWM

Credit to Luke Smith at https://lukesmith.xyz for the dwmblocks scripts

## Dependencies:
* ST

* Nitrogen

* Xbindkeys

* Pulseaudio (if you wish to use alsa just edit the `.xbindkeysrc` file accordingly)

* Nerd Fonts

## Installation:

* Clone the repo (slock is optional)

* Run `make install clean` for dwm, dmenu, and ~~slstatus~~ dwmblocks, and make the `.dwm/autostart.sh` script as well as the statusbar scripts located in `.dwm/statusbar/` executable with `chmod`

* Login to dwm using `exec dwm` in your `.xinitrc` or create a desktop file for it if you use a graphical login screen

## Keybinds:
* `ALT + ENTER`: Terminal (ST)

* `ALT + SPACE`: dmenu

* `ALT + SHIFT + ENTER`: Promote window from stack to master

* `ALT + SHIFT + SPACE`: Toggle if a window is floating or not

* `ALT + U`: Center master mode

* `ALT + O`: Center floating mode

* `ALT + F`: Floating mode

* `ALT + W`: firefox

* `ALT + Q`: scratchpad (uses ST by default)

* `ALT + SHIFT + C`: Kill (logout of) dwm

* `ALT + SHIFT Q`: Kill focused window

* `ALT + LEFT CLICK`: Move floating window

* `ALT + RIGHT CLICK`: Resize floating window
