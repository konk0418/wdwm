nitrogen --restore &
dwmblocks &
compton -b --backend glx --vsync opengl --paint-on-overlay &
xbindkeys &
# enable scrolling with trackpoint and middle button (OpenBSD Only)
#xinput set-prop "/dev/wsmouse" "WS Pointer Wheel Emulation" 1

#xinput set-prop "/dev/wsmouse" "WS Pointer Wheel Emulation Button" 2

#xinput set-prop "/dev/wsmouse" "WS Pointer Wheel Emulation Axes" 6 7 4 5

# Increase Trackpoint Sensitivity on linux
xinput --set-prop "TPPS/2 IBM TrackPoint" "Coordinate Transformation Matrix" 5 0 0 0 5 0 0 0 1
xinput -disable "Synaptics TM3276-022"
setxkbmap -option caps:escape
redshift -l 39.5257:-77.9716 -m vidmode
