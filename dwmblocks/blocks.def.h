//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
  //	{"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0},
        {" | ", "$HOME/.dwm/statusbar/sb-volume",                                   0,             1},
        {"", "$HOME/.dwm/statusbar/sb-battery",                                  5,             2},
//	{"", "$HOME/.dwm/statusbar/sb-internet",                                     5,             0},
	{"", "date +'%a, %B %d, %I:%M %p'",					 5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
